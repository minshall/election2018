* some parsing of US Federal Election Commission data

this is very rough code.  possibly only i can make use of it.

this requires the use of emacs/org-mode (in addition to R, etc.).  the
likelihood of this working on anything other than Unix/Linux is very
low.

and, it is likely that the code does *not* interpret the FEC data
completely correctly, attributing some cash flows to the wrong
committee, or ignoring some (relevant) cash flows entirely.

given the correct directory structure (from the current directory),
*fectch()* will download the complement of FEC files.

one configures the races of interest in an orgmode table, "*oi*".

*fecinit()* will load everything, filtering out records not of interest
(based on the "*oi*" table).

then, you could try something like

: byamount(c("katie porter", "mimi walters"), exclude="iex", asfrac=FALSE, limit=c(0,3000))

it is embarrassing how unpolished it is; so, only of interest if your
interest is intense!
